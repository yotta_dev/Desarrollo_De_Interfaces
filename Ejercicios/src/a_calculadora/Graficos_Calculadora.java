package a_calculadora;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import java.awt.SystemColor;

/**
 * Desarrollado para la Clase de Desarrollo de Interfaces de Coralio Cortes
 * 
 * @author Yotta_Dev
 */

public class Graficos_Calculadora extends JFrame {

	private final JPanel contentPane;

	//Instanciación de un Objecto de la Clase Operaciones_Calculadora 
	//Para poder hacer llamadas a los métodos existentes en dicha clase.
	Operaciones_Calculadora operacion = new Operaciones_Calculadora();

	//ArrayList en el que almaceno los operadores y los operandos para posteriormente
	//pasar como argumento a la clase Operaciones_Calculadora, para obtener el resultado
	//de la operación.
	ArrayList<String> bloqueDeOperacion = new ArrayList<>();

	//Entero FASE desde el cual controlo las distintas fases por las que pasa 
	//el programa
	int FASE;

	//Estos dos enteros los utilizo para almacenar el numero de veces que 
	//los botones números y operadores han sido pulsados y basándonos en eso realizar 
	//determinados conjuntos de acciones.
	int cntPulsacionNumeros, cntPulsacionOperadores;

	//Pantalla desde la que visualizamos texto
	private JTextField jtextFieldPantalla;

	//BOTONES NúMERO
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JButton button7;
	private JButton button8;
	private JButton button9;
	private JButton button0;

	//BOTONES OPERADORES
	private final JButton buttonSumar;
	private final JButton buttonRestar;
	private final JButton buttonMultiplicar;
	private final JButton buttonDividir;

	//BOTONES DE ACCIONES
	private JButton buttonCancelar;
	private JButton buttonResultado;

	/**
	 * Launch the application.
	 *
	 * @param args
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Graficos_Calculadora frame = new Graficos_Calculadora();
					frame.setVisible(true);
					frame.setLocationRelativeTo(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Graficos_Calculadora() {
		setTitle("Calculadora By Yotta_Dev");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 265, 368);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		jtextFieldPantalla = new JTextField();
		jtextFieldPantalla.setBackground(SystemColor.text);
		jtextFieldPantalla.setEditable(false);
		jtextFieldPantalla.setHorizontalAlignment(SwingConstants.RIGHT);
		jtextFieldPantalla.setBounds(28, 25, 208, 39);
		contentPane.add(jtextFieldPantalla);
		jtextFieldPantalla.setColumns(10);

		//COMIENZO BLOQUE BUTTONS NUMEROS
		//BUTTON 1
		button1 = new JButton("1");
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				mostrarNumeroEnPantalla(button1.getText());
				cntPulsacionNumeros++;
				controldeOperaciones();
			}
		});
		button1.setBounds(28, 86, 42, 45);
		contentPane.add(button1);
		//FIN BUTTON 1

		//BUTTON 2
		button2 = new JButton("2");
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				mostrarNumeroEnPantalla(button2.getText());
				cntPulsacionNumeros++;
				controldeOperaciones();
			}
		});
		button2.setBounds(82, 86, 42, 45);
		contentPane.add(button2);
		//FIN BUTTON 2

		//BUTTON 3
		button3 = new JButton("3");
		button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				mostrarNumeroEnPantalla(button3.getText());
				cntPulsacionNumeros++;
				controldeOperaciones();
			}
		});
		button3.setBounds(138, 86, 42, 45);
		contentPane.add(button3);
		//FIN BUTTON 3

		//BUTTON 4
		button4 = new JButton("4");
		button4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				mostrarNumeroEnPantalla(button4.getText());
				cntPulsacionNumeros++;
				controldeOperaciones();
			}
		});
		button4.setBounds(28, 146, 42, 45);
		contentPane.add(button4);
		//FIN BUTTON 4

		//BUTTON 5
		button5 = new JButton("5");
		button5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				mostrarNumeroEnPantalla(button5.getText());
				cntPulsacionNumeros++;
				controldeOperaciones();
			}
		});
		button5.setBounds(82, 146, 42, 45);
		contentPane.add(button5);
		//FIN BUTTON 5

		//BUTTON 6
		button6 = new JButton("6");
		button6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				mostrarNumeroEnPantalla(button6.getText());
				cntPulsacionNumeros++;
				controldeOperaciones();
			}
		});
		button6.setBounds(138, 146, 42, 45);
		contentPane.add(button6);
		//FIN BUTTON 6

		//BUTTON 7
		button7 = new JButton("7");
		button7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				mostrarNumeroEnPantalla(button7.getText());
				cntPulsacionNumeros++;
				controldeOperaciones();
			}
		});
		button7.setBounds(28, 203, 42, 45);
		contentPane.add(button7);
		//FIN BUTTON 7

		//BUTTON 8
		button8 = new JButton("8");
		button8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				mostrarNumeroEnPantalla(button8.getText());
				cntPulsacionNumeros++;
				controldeOperaciones();
			}
		});
		button8.setBounds(82, 203, 42, 45);
		contentPane.add(button8);
		//FIN BUTTON 8

		//BUTTON 9
		button9 = new JButton("9");
		button9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				mostrarNumeroEnPantalla(button9.getText());
				cntPulsacionNumeros++;
				controldeOperaciones();
			}
		});
		button9.setBounds(138, 203, 42, 45);
		contentPane.add(button9);
		//FIN BUTTON 9

		//BUTTON 0
		button0 = new JButton("0");
		button0.setEnabled(false);
		button0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				mostrarNumeroEnPantalla(button0.getText());
				cntPulsacionNumeros++;
				controldeOperaciones();
			}

		});
		button0.setBounds(28, 264, 42, 45);
		contentPane.add(button0);
		//FIN BUTTON 0

		//BUTTON CANCELAR
		buttonCancelar = new JButton("C");
		buttonCancelar.setEnabled(false);
		buttonCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				cancelar(false);

			}
		});
		buttonCancelar.setFont(new Font("Dialog", Font.BOLD, 10));
		buttonCancelar.setBounds(82, 264, 42, 45);
		contentPane.add(buttonCancelar);
		//FIN BUTTON CANCELAR

		//BLOQUE DE BUTTONS OPERADORES
		//BUTTON RESULTADO
		buttonResultado = new JButton("=");
		buttonResultado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				buttonCancelar.setEnabled(true);
				habilitarDeshabilitarNumeros(false);
				button0.setEnabled(false);
				habilitarButtonOperadoresSiProcede(FASE = 0);

				buttonResultado.setEnabled(false);

				bloqueDeOperacion.add(jtextFieldPantalla.getText());
				jtextFieldPantalla.setText(operacion.realizarCalculo(bloqueDeOperacion));
				bloqueDeOperacion.clear();

				cntPulsacionNumeros = 0;
				cntPulsacionOperadores = 0;
			}
		});
		buttonResultado.setEnabled(false);
		buttonResultado.setFont(new Font("Dialog", Font.BOLD, 10));
		buttonResultado.setBounds(138, 264, 42, 45);
		contentPane.add(buttonResultado);
		//FIN BUTTON RESULTADO

		//BUTTON SUMAR
		buttonSumar = new JButton("+");
		buttonSumar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionPulsarOperador("+");
			}
		});
		buttonSumar.setEnabled(false);
		buttonSumar.setFont(new Font("Dialog", Font.BOLD, 10));
		buttonSumar.setBounds(194, 86, 42, 45);
		contentPane.add(buttonSumar);
		//FIN BUTTON SUMAR

		//BUTTON RESTAR
		buttonRestar = new JButton("-");
		buttonRestar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionPulsarOperador("-");
			}
		});
		buttonRestar.setEnabled(false);
		buttonRestar.setFont(new Font("Dialog", Font.BOLD, 13));
		buttonRestar.setBounds(194, 146, 42, 45);
		contentPane.add(buttonRestar);
		//FIN BUTTON RESTAR

		//BUTTON MULTIPLICAR
		buttonMultiplicar = new JButton("X");
		buttonMultiplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionPulsarOperador("*");
			}
		});
		buttonMultiplicar.setEnabled(false);
		buttonMultiplicar.setFont(new Font("Dialog", Font.BOLD, 9));
		buttonMultiplicar.setBounds(194, 203, 42, 45);
		contentPane.add(buttonMultiplicar);
		//FIN BUTTON MULTIPLICAR

		//BUTTON DIVIDIR
		buttonDividir = new JButton("/");
		buttonDividir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				accionPulsarOperador("/");
			}
		});
		buttonDividir.setEnabled(false);
		buttonDividir.setFont(new Font("Dialog", Font.BOLD, 10));
		buttonDividir.setBounds(194, 264, 42, 45);
		contentPane.add(buttonDividir);
		//FIN BUTTON DIVIDIR

		//FIN BLOQUE BUTTONS OPERADORES
	}

	/* Metodos Para el Control del Funcionamiento de la Interfaz Gráfica */
	/**
	 * Este método al ser llamado limpiara el contenido existente en la pantalla
	 * por una cadena de texto vacía.
	 */
	private void limpiarPantallaJTextField() {
		jtextFieldPantalla.setText("");
	}

	/**
	 * A este método se le pasa como parametro un String que contiene un número
	 * este numero el método se encarga de mostrarlo en pantalla.
	 *
	 * @param numero
	 */
	private void mostrarNumeroEnPantalla(String numero) {
		if (jtextFieldPantalla.getText().length() >= 1) {

			jtextFieldPantalla.setText(jtextFieldPantalla.getText() + numero);

		} else {

			jtextFieldPantalla.setText(numero);
		}
	}

	/**
	 * En este método controlo la acción de pulsar el Button Operador Al ser
	 * pulsado almaceno el numero existente en la pantalla y el operador pulsado
	 * en el ArrayList Bloque de Operaciones, tras eso limpio la pantalla
	 * deshabilito el 0, hago un conteo de que se ha pulsado el Button Operador
	 * y vuelo a la FASE 0, el parámetro que le pasamos es el que determina la
	 * la operación a realizar.
	 *
	 * @param operador
	 */
	private void accionPulsarOperador(String operador) {
		bloqueDeOperacion.add(jtextFieldPantalla.getText());
		bloqueDeOperacion.add(operador);

		limpiarPantallaJTextField();

		button0.setEnabled(false);

		cntPulsacionOperadores++;

		habilitarButtonOperadoresSiProcede(FASE = 0);
	}

	/**
	 * En este método manejo el Control de las Operaciones y cuando los botones
	 * de Operación deben de estar activados o no, basándonos en un conteo
	 * realizado en el número de veces que los Button Operador y los Números han
	 * sido pulsados.
	 */
	private void controldeOperaciones() {
		if (cntPulsacionOperadores >= 1) {

			habilitarButtonOperadoresSiProcede(FASE = 0);
			habilitarButtonResultadoSiProcede();
			button0.setEnabled(true);

		} else {

			habilitarButtonOperadoresSiProcede(FASE = 1);

		}

		if (cntPulsacionNumeros >= 1) {
			button0.setEnabled(true);
		}

	}

	/**
	 * Método al que llamo cuando quiero preguntarme si el Button Resultado debe
	 * esta habilitado o no
	 */
	private void habilitarButtonResultadoSiProcede() {

		if (FASE == 0) {
			buttonResultado.setEnabled(true);
		}
	}

	/**
	 * Metodo en el que controlo si los Operadores deben estar habilitados o no
	 * en Base a la FASE en la que nos encontramos en este momento, FASE 1
	 * Mostrar FASE 0 no mostrar
	 *
	 * @param FASE
	 */
	private void habilitarButtonOperadoresSiProcede(int FASE) {

		if (FASE == 1) {
			buttonSumar.setEnabled(true);

			buttonRestar.setEnabled(true);

			buttonMultiplicar.setEnabled(true);

			buttonDividir.setEnabled(true);

			buttonCancelar.setEnabled(true);

		}

		if (FASE == 0) {
			buttonSumar.setEnabled(false);

			buttonRestar.setEnabled(false);

			buttonMultiplicar.setEnabled(false);

			buttonDividir.setEnabled(false);

			buttonCancelar.setEnabled(true);
		}
	}

	/**
	 * Método para mostrar o Deshabilitar todos los Button Numero
	 *
	 * @param activador
	 */
	private void habilitarDeshabilitarNumeros(Boolean activador) {

		button1.setEnabled(activador);
		button2.setEnabled(activador);
		button3.setEnabled(activador);
		button4.setEnabled(activador);
		button5.setEnabled(activador);
		button6.setEnabled(activador);
		button7.setEnabled(activador);
		button8.setEnabled(activador);
		button9.setEnabled(activador);

	}

	/**
	 * Método de realizar toda la cancelación de acciones que se van a realizar
	 * o que están siendo realizadas, se le pasa un Booleano con la acción a
	 * realizar en este caso deberá ser siempre FALSE para que realice la acción
	 * de Cancelar
	 *
	 * @param cancelador
	 */
	private void cancelar(boolean cancelador) {
		jtextFieldPantalla.setText("");

		bloqueDeOperacion.clear();

		buttonSumar.setEnabled(cancelador);
		buttonRestar.setEnabled(cancelador);
		buttonMultiplicar.setEnabled(cancelador);
		buttonDividir.setEnabled(cancelador);

		buttonResultado.setEnabled(cancelador);

		button0.setEnabled(cancelador);

		habilitarDeshabilitarNumeros(!cancelador);

		cntPulsacionNumeros = 0;
		cntPulsacionOperadores = 0;

		buttonCancelar.setEnabled(cancelador);
	}
}
