package a_calculadora;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 * Desarrollado para la Clase de Desarrollo de Interfaces de Coralio Cortes
 * 
 * @author Yotta_Dev
 */
public class Operaciones_Calculadora {
	
	public String realizarCalculo(ArrayList<String> bloqueDeOperacion) {
	
		String resultadoAMostrar;
		
        double operandoIzq, operandoDech, resultado = 0;
        
        DecimalFormat formateador = new DecimalFormat("#.##");
        
        operandoIzq = Double.valueOf(bloqueDeOperacion.get(0));
        
        operandoDech = Double.valueOf(bloqueDeOperacion.get(2));
        
        switch (bloqueDeOperacion.get(1)) {
            case "+":
                resultado = operandoIzq + operandoDech;
                break;
            case "-":
                resultado = operandoIzq - operandoDech;
                break;
            case "*":
                resultado = operandoIzq * operandoDech;
                break;
            case "/":
                resultado = operandoIzq / operandoDech;
                break;
        }
        resultadoAMostrar = String.valueOf(formateador.format(resultado));	
        
        return resultadoAMostrar; 
	}

}
