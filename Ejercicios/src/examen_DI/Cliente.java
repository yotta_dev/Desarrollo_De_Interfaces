/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package examen_DI;

/**
 *
 * @author alumno
 */
public class Cliente {
	
	String codigo;
	String nif;
	String apellidos;
	String nombre;
	String domicilio;
	String codigo_postal;
	String localidad;
	String telefono;
	String movil;
	String fax;

	public Cliente(String codigo, String nif, String apellidos, String nombre, String domicilio, String codigo_postal, String localidad, String telefono, String movil, String fax) {
		this.codigo = codigo;
		this.nif = nif;
		this.apellidos = apellidos;
		this.nombre = nombre;
		this.domicilio = domicilio;
		this.codigo_postal = codigo_postal;
		this.localidad = localidad;
		this.telefono = telefono;
		this.movil = movil;
		this.fax = fax;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getCodigo_postal() {
		return codigo_postal;
	}

	public void setCodigo_postal(String codigo_postal) {
		this.codigo_postal = codigo_postal;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}
	
	
}
