CREATE DATABASE ALMACEN;

USE ALMACEN;

CREATE TABLE ClientesAltas (
	codigo VARCHAR(6),
	nif VARCHAR(9),
	apellidos VARCHAR(35),
	nombre VARCHAR(15),
	domicilio VARCHAR(40),
	codigo_postal VARCHAR(5),
	localidad VARCHAR(20),
	telefono VARCHAR(9),
	movil VARCHAR(9),
	fax VARCHAR(9),
	email VARCHAR(20),
	total_ventas FLOAT(5),
	PRIMARY KEY (codigo)
);

INSERT INTO ClientesAltas(codigo,nif,apellidos,nombre,domicilio,codigo_postal,localidad,telefono,movil,fax,email,total_ventas)
VALUES ('000000','06018454K','Salazar','Yehoshua','C/Real 37','28400','Villalba','918512759','674672616','999999999','test1@gmail.com',100);

INSERT INTO ClientesAltas(codigo,nif,apellidos,nombre,domicilio,codigo_postal,localidad,telefono,movil,fax,email,total_ventas)
VALUES ('111111','56987231Y','Cuervas','Borja','C/Petrut 45','32569','Boalo','916897543','6985362124','111111111','borja@gmail.com',50);

CREATE TABLE ClientesBajas (
        id_baja INTEGER (10)AUTO_INCREMENT,
	codigo VARCHAR(6),
	nif VARCHAR(9),
	apellidos VARCHAR(35),
	nombre VARCHAR(15),
	domicilio VARCHAR(40),
	codigo_postal VARCHAR(5),
	localidad VARCHAR(20),
	telefono VARCHAR(9),
	movil VARCHAR(9),
	fax VARCHAR(9),
	email VARCHAR(20),
	total_ventas FLOAT(5),
	PRIMARY KEY (id_baja)
);

CREATE TABLE ClientesModificaciones (
        id_modificacion INTEGER (10)AUTO_INCREMENT,
	codigo VARCHAR(6),
	nif VARCHAR(9),
	apellidos VARCHAR(35),
	nombre VARCHAR(15),
	domicilio VARCHAR(40),
	codigo_postal VARCHAR(5),
	localidad VARCHAR(20),
	telefono VARCHAR(9),
	movil VARCHAR(9),
	fax VARCHAR(9),
	email VARCHAR(20),
	total_ventas FLOAT(5),
	PRIMARY KEY (id_modificacion)
);

CREATE TABLE ConsultasPorCodigo (
	codigo VARCHAR(6),
	nif VARCHAR(9),
	apellidos VARCHAR(35),
	nombre VARCHAR(15),
	domicilio VARCHAR(40),
	codigo_postal VARCHAR(5),
	localidad VARCHAR(20),
	telefono VARCHAR(9),
	movil VARCHAR(9),
	fax VARCHAR(9),
	email VARCHAR(20),
	total_ventas FLOAT(5),
	PRIMARY KEY (codigo)
);

UPDATE " + tabla + "SET apellidos=?,nombre=?,domicilio=?,codigo_postal=?,localidad=?,telefono=?,movil=?,fax=?,email=?,total_ventas=? WHERE codigo = ?;

UPDATE ClientesModificaciones SET apellidos=ZZzzzzzzz WHERE codigo=000000;

//Consulta Informe
select codigo,apellidos,nombre,total_ventas from clientesAltas ORDER BY codigo;

C:\Users\TheSlayeOne1\Desktop\Repositorios\Desarrollo_De_Interfaces\Ejercicios\src\h_gestion_De_Almacenes_Avanzado_Reportes

SET autocommit=0;