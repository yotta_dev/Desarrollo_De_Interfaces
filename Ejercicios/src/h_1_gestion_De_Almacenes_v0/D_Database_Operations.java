/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package h_1_gestion_De_Almacenes_v0;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.ResultSet;
import java.awt.event.KeyEvent;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Yotta_Dev
 */
public class D_Database_Operations {

    C_Operating_Methods operating_Methods = new C_Operating_Methods(); //Objeto de la Clase que tiene los Métodos Operadores

    static Connection connection; // Conexion con la BD

    public void connect() {

        try {
            Class.forName("com.mysql.jdbc.Driver");

            String cadenaConexion = "jdbc:mysql://localhost:3306/almacen";
            String usuario = "root";
            String password = "manager";

            connection = (Connection) DriverManager.getConnection(cadenaConexion, usuario, password);

            System.out.println("Conectado");

        } catch (ClassNotFoundException e) {

            operating_Methods.mostrarMensajeError(e.getMessage(), "Class No Encontrada");

        } catch (SQLException ex) {

            operating_Methods.mostrarMensajeError(ex.getMessage(), "Error BBDD");
            System.exit(0);

        }

    }

    public boolean consultarSiClienteExiste(String codigo, String tabla) {

        try {
            String sentenciaSQL = "SELECT * FROM " + tabla + " WHERE codigo = ?";

            PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
            preparedStatement.setString(1, codigo);
            ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();

            if (resultSet.next()) {//Existe
                return true;
            } else {//No Existe
                return false;
            }
        } catch (SQLException exception) {

            operating_Methods.mostrarMensajeError(exception.getMessage(), "Error BBDD");
            return false;
        }
    }

    public boolean consultarCodigoCliente(String codigo, String modo, KeyEvent evt) throws SQLException {

        if ((int) evt.getKeyChar() == 10) {
            //Variables Globales a las 4 Consultas
            PreparedStatement preparedStatement = null;
            ResultSet resultSet;
            String sentenciaSQL;

            switch (modo) {

                case "Altas_de_Clientes":
                    //Bloque de Comprobacion de la Tabla de Altas
                    sentenciaSQL = "SELECT * FROM ClientesAltas WHERE codigo = ?";
                    preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
                    preparedStatement.setString(1, codigo);
                    resultSet = (ResultSet) preparedStatement.executeQuery();

                    if (resultSet.next()) {//Existe
                        return true;
                    } else {//No Existe
                        return false;
                    }

                case "Bajas_de_Clientes":
                    //Bloque de Comprobacion de la Tabla de Bajas
                    sentenciaSQL = "SELECT * FROM ClientesBajas WHERE codigo = ?";
                    preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
                    preparedStatement.setString(1, codigo);
                    resultSet = (ResultSet) preparedStatement.executeQuery();

                    if (resultSet.next()) {//Existe
                        return true;
                    } else {//No Existe
                        return false;
                    }

                case "Modificaciones_de_Clientes":
                    //Bloque de Comprobacion de la Tabla de Modificaciones
                    sentenciaSQL = "SELECT * FROM ClientesModificaciones WHERE codigo = ?";
                    preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
                    preparedStatement.setString(1, codigo);
                    resultSet = (ResultSet) preparedStatement.executeQuery();
                    if (resultSet.next()) {//Existe
                        return true;
                    } else {//No Existe
                        return false;
                    }

                case "Consultas_Por_Codigo":
                    //Bloque de Comprobacion de la Tabla de Consultas Por Codigo
                    sentenciaSQL = "SELECT * FROM ConsultasPorCodigo WHERE codigo = ?";
                    preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
                    preparedStatement.setString(1, codigo);
                    resultSet = (ResultSet) preparedStatement.executeQuery();
                    if (resultSet.next()) {//Existe
                        return false;
                    } else {//No Existe
                        return true;
                    }
            }

        }
        //En caso de que no haya entrado por ninguno de los IF anteriores
        return false;
    }

    public void altaCliente(H_Cliente cliente, String tabla) {

        try {

            String insert;
            PreparedStatement prepareStatement;

            switch (tabla) {
                case "ClientesAltas":
                    insert = "INSERT INTO " + tabla + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";

                    prepareStatement = (PreparedStatement) connection.prepareStatement(insert);

                    prepareStatement.setString(1, cliente.getCodigo());
                    prepareStatement.setString(2, cliente.getNif());
                    prepareStatement.setString(3, cliente.getApellidos());
                    prepareStatement.setString(4, cliente.getNombre());
                    prepareStatement.setString(5, cliente.getDomicilio());
                    prepareStatement.setString(6, cliente.getCodigo_postal());
                    prepareStatement.setString(7, cliente.getLocalidad());
                    prepareStatement.setString(8, cliente.getTelefono());
                    prepareStatement.setString(9, cliente.getMovil());
                    prepareStatement.setString(10, cliente.getFax());
                    prepareStatement.setString(11, cliente.getEmail());
                    prepareStatement.setFloat(12, cliente.getTotal_ventas());

                    prepareStatement.executeUpdate();
                    break;

                case "ClientesBajas":
                    insert = "INSERT INTO " + tabla + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

                    prepareStatement = (PreparedStatement) connection.prepareStatement(insert);

                    prepareStatement.setString(1, null);
                    prepareStatement.setString(2, cliente.getCodigo());
                    prepareStatement.setString(3, cliente.getNif());
                    prepareStatement.setString(4, cliente.getApellidos());
                    prepareStatement.setString(5, cliente.getNombre());
                    prepareStatement.setString(6, cliente.getDomicilio());
                    prepareStatement.setString(7, cliente.getCodigo_postal());
                    prepareStatement.setString(8, cliente.getLocalidad());
                    prepareStatement.setString(9, cliente.getTelefono());
                    prepareStatement.setString(10, cliente.getMovil());
                    prepareStatement.setString(11, cliente.getFax());
                    prepareStatement.setString(12, cliente.getEmail());
                    prepareStatement.setFloat(13, cliente.getTotal_ventas());

                    prepareStatement.executeUpdate();
                    break;

                case "ClientesModificaciones":
                    insert = "INSERT INTO " + tabla + " VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)";

                    prepareStatement = (PreparedStatement) connection.prepareStatement(insert);

                    prepareStatement.setString(1, null);
                    prepareStatement.setString(2, cliente.getCodigo());
                    prepareStatement.setString(3, cliente.getNif());
                    prepareStatement.setString(4, cliente.getApellidos());
                    prepareStatement.setString(5, cliente.getNombre());
                    prepareStatement.setString(6, cliente.getDomicilio());
                    prepareStatement.setString(7, cliente.getCodigo_postal());
                    prepareStatement.setString(8, cliente.getLocalidad());
                    prepareStatement.setString(9, cliente.getTelefono());
                    prepareStatement.setString(10, cliente.getMovil());
                    prepareStatement.setString(11, cliente.getFax());
                    prepareStatement.setString(12, cliente.getEmail());
                    prepareStatement.setFloat(13, cliente.getTotal_ventas());

                    prepareStatement.executeUpdate();
                    break;
            }

            connection.commit();

        } catch (SQLException ex) {

            //operating_Methods.mostrarMensajeError(ex.getMessage(), "Error BBDD");
            System.out.println(ex.getMessage());
        }

    }

    public void bajaCliente(String codigo, String tabla) {

        try {

            PreparedStatement preparedStatement = null;

            String sentenciaSQL = "DELETE FROM " + tabla + " WHERE codigo = ?";

            preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);
            preparedStatement.setString(1, codigo);
            preparedStatement.executeUpdate();

        } catch (SQLException exception) {

            operating_Methods.mostrarMensajeError(exception.getMessage(), "Error SQL");

        }
    }

    public void modificacionCliente(H_Cliente cliente, String tabla) {

        try {
            String sentenciaSQL = "UPDATE " + tabla + " SET apellidos=?,nombre=?,domicilio=?,codigo_postal=?,localidad=?,telefono=?,movil=?,fax=?,email=?,total_ventas=? WHERE codigo=?";

            PreparedStatement prepareStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);

            prepareStatement.setString(1, cliente.getApellidos());
            prepareStatement.setString(2, cliente.getNombre());
            prepareStatement.setString(3, cliente.getDomicilio());
            prepareStatement.setString(4, cliente.getCodigo_postal());
            prepareStatement.setString(5, cliente.getLocalidad());
            prepareStatement.setString(6, cliente.getTelefono());
            prepareStatement.setString(7, cliente.getMovil());
            prepareStatement.setString(8, cliente.getFax());
            prepareStatement.setString(9, cliente.getEmail());
            prepareStatement.setFloat(10, cliente.getTotal_ventas());

            prepareStatement.setString(11, cliente.getCodigo());

            prepareStatement.executeUpdate();

            System.out.println("UPDATE");

        } catch (SQLException exception) {

            operating_Methods.mostrarMensajeError(exception.getMessage(), "Error SQL");

        }

    }

    public static H_Cliente obtenerObjetoCliente(String codigo, String tabla) {

        H_Cliente cliente = null;

        try {

            String sentenciaSQL = "SELECT * FROM " + tabla + " WHERE codigo = ?";

            PreparedStatement preparedStatement = (PreparedStatement) connection.prepareStatement(sentenciaSQL);

            preparedStatement.setString(1, codigo);

            ResultSet resultSet = (ResultSet) preparedStatement.executeQuery();

            if (resultSet.next()) {
                cliente = new H_Cliente(
                        resultSet.getString(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4),
                        resultSet.getString(5),
                        resultSet.getString(6),
                        resultSet.getString(7),
                        resultSet.getString(8),
                        resultSet.getString(9),
                        resultSet.getString(10),
                        resultSet.getString(11),
                        resultSet.getFloat(12));

                return cliente;

            }

        } catch (SQLException exception) {

            System.out.println(exception.getMessage());

        }
        return cliente;
    }

    public void generarReporteSimple(String reporte) {

        try {

            JasperPrint jasperPrint = JasperFillManager.fillReport("src/h_2_gestion_De_Almacenes_Reportes_v0/"+reporte+".jasper", null, connection);

            JasperViewer jasperViewer = new JasperViewer(jasperPrint,false);                       
            
            jasperViewer.setTitle(reporte);     
            
            jasperViewer.setAlwaysOnTop(true);

            jasperViewer.setVisible(true);
            

        } catch (JRException ex) {

            operating_Methods.mostrarMensajeError(ex.getMessage(), "Error generando Informe");

        }

    }
    
    public void generarReporteConParametros(String reporte,String codigo_1,String codigo_2) {

        try {
            Map parametros = new HashMap();
            
            parametros.put("codigo_1", codigo_1);
            
            parametros.put("codigo_2", codigo_2);
            
            JasperPrint jasperPrint = JasperFillManager.fillReport("src/h_2_gestion_De_Almacenes_Reportes_v0/"+reporte+".jasper", parametros, connection);

            JasperViewer jasperViewer = new JasperViewer(jasperPrint,false);
            
            jasperViewer.setAlwaysOnTop(true);

            jasperViewer.setTitle("Listado Entre Códigos");

            jasperViewer.setVisible(true);   
            
            

        } catch (JRException ex) {

            operating_Methods.mostrarMensajeError(ex.getMessage(), "Error generando Informe");

        }

    }

}
/*
 public void bajaPersona(String dni) throws SQLException {
 PreparedStatement ps = null;
 String prep = "DELETE FROM persona WHERE dni = ?";

 ps = cn.prepareStatement(prep);
 ps.setString(1, dni);
 ps.executeUpdate();
 }

 @Override
 public void desconectar() throws Exception {
 cn.close();
 }

 @Override
 public ArrayList<Persona> listadoPersonas(String tabla, String orderBy) throws SQLException {
 ArrayList<Persona> al = new ArrayList<Persona>();

 Statement st = cn.createStatement();
 String sql = "SELECT * FROM " + tabla + " " + orderBy;
 ResultSet rs = st.executeQuery(sql);
 while (rs.next()) {
 // Crea objeto Persona y lo añade a ArrayList
 al.add(new Persona(rs.getString("nombre"), rs.getString("CP"), rs.getString("pais"), rs.getString("email")));
 }
 return al;
 }

 @Override
 public void guardarPersona(String tabla, Persona p) throws Exception {
 //Comprueba si existe el email(clave primaria) para hacer UPDATE o INSERT
 boolean existe = consultarPersona(tabla, p.getEmail()) != null;
 String prep;
 if (existe) {
 prep = "UPDATE " + tabla + " SET nombre=?,CP=?,pais=? WHERE email=?";
 } else {
 prep = "INSERT INTO " + tabla + " VALUES (?,?,?,?)";
 }
 PreparedStatement ps = cn.prepareStatement(prep);
 ps.setString(1, p.getNombre());
 ps.setString(2, p.getCp());
 ps.setString(3, p.getPais());
 ps.setString(4, p.getEmail());
 ps.executeUpdate();
 }

 @Override
 public void borrarPersona(String tabla, String email) throws Exception {
 PreparedStatement ps = null;
 String prep = "DELETE FROM " + tabla + " WHERE email = ?";

 ps = cn.prepareStatement(prep);
 ps.setString(1, email);
 ps.executeUpdate();
 }

 @Override
 public Persona consultarPersona(String tabla, String email) throws Exception {
 // Comprueba que existe el email y devuelve objeto persona. null si no existe email
 PreparedStatement ps = null;
 String prep = "SELECT * FROM " + tabla + " WHERE email = ?";
 ResultSet rs;
 ps = cn.prepareStatement(prep);
 ps.setString(1, email);
 rs = ps.executeQuery();
 if (rs.next()) { // Existe
 // Crea objeto Persona a partir de las columnas obtenidas con SELECT.
 Persona p = new Persona(rs.getString("nombre"), rs.getString("CP"), rs.getString("pais"),
 rs.getString("email"));
 return p;
 } else { // No existe
 return null;
 }
 }
 */
