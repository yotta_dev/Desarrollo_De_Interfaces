/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package g_ventaOrdenadores_PersistenciaSecuencial;

import java.io.Serializable;

/**
 *
 * @author Yehoshua
 */
public class VentaCliente implements Serializable {
    String nombreCliente;
    int localidad;
    int procesador;
    int memoria;
    int monitor;
    int discoDuro;
    boolean opcion1,opcion2,opcion3,opcion4;
    
    public VentaCliente(String nombreCliente, int localidad, int procesador, int memoria, int monitor, int discoDuro, boolean opcion1, boolean opcion2, boolean opcion3, boolean opcion4) {
        this.nombreCliente = nombreCliente;
        this.localidad = localidad;
        this.procesador = procesador;
        this.memoria = memoria;
        this.monitor = monitor;
        this.discoDuro = discoDuro;
        this.opcion1 = opcion1;
        this.opcion2 = opcion2;
        this.opcion3 = opcion3;
        this.opcion4 = opcion4;
    }
    
    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public int getLocalidad() {
        return localidad;
    }

    public void setLocalidad(int localidad) {
        this.localidad = localidad;
    }

    public int getProcesador() {
        return procesador;
    }

    public void setProcesador(int procesador) {
        this.procesador = procesador;
    }

    public int getMemoria() {
        return memoria;
    }

    public void setMemoria(int memoria) {
        this.memoria = memoria;
    }

    public int getMonitor() {
        return monitor;
    }

    public void setMonitor(int monitor) {
        this.monitor = monitor;
    }

    public int getDiscoDuro() {
        return discoDuro;
    }

    public void setDiscoDuro(int discoDuro) {
        this.discoDuro = discoDuro;
    }

    public boolean isOpcion1() {
        return opcion1;
    }

    public void setOpcion1(boolean opcion1) {
        this.opcion1 = opcion1;
    }

    public boolean isOpcion2() {
        return opcion2;
    }

    public void setOpcion2(boolean opcion2) {
        this.opcion2 = opcion2;
    }

    public boolean isOpcion3() {
        return opcion3;
    }

    public void setOpcion3(boolean opcion3) {
        this.opcion3 = opcion3;
    }

    public boolean isOpcion4() {
        return opcion4;
    }

    public void setOpcion4(boolean opcion4) {
        this.opcion4 = opcion4;
    }

	@Override
	public String toString() {
		return "VentaCliente{" + "nombreCliente=" + nombreCliente + ", localidad=" + localidad + ", procesador=" + procesador + ", memoria=" + memoria + ", monitor=" + monitor + ", discoDuro=" + discoDuro + ", opcion1=" + opcion1 + ", opcion2=" + opcion2 + ", opcion3=" + opcion3 + ", opcion4=" + opcion4 + '}';
	}
	
	
}
