/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b_gestion_De_Almacenes;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author Yehoshua
 */
public class Gestion_De_Almacenes_Methods {

	//COMIENZO DE VARIABLES DECLARADAS POR EL DESARROLLADOR
	Codigos_De_Error mensajeAMostrar = new Codigos_De_Error();
    //FIN DE VARIABLES DECLARADAS POR EL DESARROLLADOR

	//COMIENZO MÉTODOS CREADOS POR EL DESARROLLADOR
	/**
	 * Este es el Método principal de la clase Gestión de Almacenes Methods, y
	 * se encarga de validar todos los campos JTexfield uno a uno y en el caso
	 * de encontrar algún campo que no cumple con los requisitos lanzar un
	 * código de error en forma de String
	 *
	 * @param jTextFieldApellidos
	 * @param jTextFieldCodigo
	 * @param jTextFieldCodigoPostal
	 * @param jTextFieldDomicilio
	 * @param jTextFieldEmail
	 * @param jTextFieldFax
	 * @param jTextFieldLocalidad
	 * @param jTextFieldMovil
	 * @param jTextFieldNIFLetra
	 * @param jTextFieldNIFNumeros
	 * @param jTextFieldNombre
	 * @param jTextFieldTelefono
	 * @param jTextFieldTotalVentas
	 */
	public ArrayList<Integer> validarDatosIntroducidos(JTextField jTextFieldApellidos, JTextField jTextFieldCodigo, JTextField jTextFieldCodigoPostal, JTextField jTextFieldDomicilio, JTextField jTextFieldEmail, JTextField jTextFieldFax, JTextField jTextFieldLocalidad, JTextField jTextFieldMovil, JTextField jTextFieldNIFLetra, JTextField jTextFieldNIFNumeros, JTextField jTextFieldNombre, JTextField jTextFieldTelefono, JTextField jTextFieldTotalVentas) {
		ArrayList<Integer> erroresCometidos = new ArrayList<Integer>();

		if (!jTextFieldCodigo.getText().matches("[a-zA-Z0-9]{6}")) {
			erroresCometidos.add(1);
		}
		if (!jTextFieldNIFNumeros.getText().matches("[\\d]{8}")) {
			erroresCometidos.add(2);
		}
		if (!jTextFieldNombre.getText().matches("([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú.]+[\\s]*)+$")) {
			erroresCometidos.add(3);
		}
		if (!jTextFieldApellidos.getText().matches("([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú.-]+[\\s]*)+$")) {
			erroresCometidos.add(4);
		}
		if (!jTextFieldDomicilio.getText().matches("[A-Z][A-z0-9ªºñÑáéíóúÁÉÍÓÚ/\\.,;: -]+{1,30}")) {
			erroresCometidos.add(5);
		}
		if (!jTextFieldCodigoPostal.getText().matches("[\\d]{5}")) {
			erroresCometidos.add(6);
		}
		if (!jTextFieldLocalidad.getText().matches("[A-Z][A-zñÑáéíóúÁÉÍÓÚ., -]+{1,30}")) {
			erroresCometidos.add(7);
		}
		if (!jTextFieldTelefono.getText().matches("[0-9]{1}([\\d]{2}[-]*){3}[\\d]{2}$")) {
			erroresCometidos.add(8);
		}
		if (!jTextFieldMovil.getText().matches("[0-9]{1}([\\d]{2}[-]*){3}[\\d]{2}$")) {
			erroresCometidos.add(9);
		}
		if (!jTextFieldFax.getText().matches("[0-9]{1}([\\d]{2}[-]*){3}[\\d]{2}$")) {
			erroresCometidos.add(10);
		}
		if (!jTextFieldEmail.getText().toLowerCase().matches("[A-Za-z0-9.-]{1,17}[@][a-z0-9-]{1,17}[.][a-z0-9_-]{1,6}[.]?[a-z0-9_-]{0,6}")) {
			erroresCometidos.add(11);
		}
		if (erroresCometidos.isEmpty()) {
			jTextFieldTotalVentas.setText("0");
			agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldCodigo);
		}
		return erroresCometidos;
	}

	/**
	 * Éste método se apoya en el Método que valida los Datos para construir en
	 * un String toda la cadena de errores Cometidos, se le pasa como parámetro
	 * un arraylist de enteros, que almacena los códigos de error cometidos, y
	 * en base a cada código de error, se almacena en la cadena de recopilación
	 * de errores y posteriormente se muestra como error. código de error en
	 * forma de String
	 *
	 * @param aThis
	 * @param erroresCometidos
	 *
	 */
	public void mostrarMensajeResultado(Gestion_De_Almacenes_Graphics aThis, ArrayList<Integer> erroresCometidos, JPanel jPanelMostrarErrores, JLabel jLabelMasDetalles, JTextArea jTextAreaErrores) {

		ArrayList<String> recopilacionDeErrores = new ArrayList<String>();

		if (erroresCometidos.size() == 0) {
			JOptionPane.showMessageDialog(aThis, "Se ha rellenado Correctamente", "Gestión de Alta",
					JOptionPane.INFORMATION_MESSAGE);
			
			cancelarIntroduccionDatos(Gestion_De_Almacenes_Graphics.jTextFieldCodigo, Gestion_De_Almacenes_Graphics.jTextFieldApellidos, Gestion_De_Almacenes_Graphics.jTextFieldCodigoPostal, Gestion_De_Almacenes_Graphics.jTextFieldDomicilio, Gestion_De_Almacenes_Graphics.jTextFieldEmail, Gestion_De_Almacenes_Graphics.jTextFieldFax, Gestion_De_Almacenes_Graphics.jTextFieldLocalidad, Gestion_De_Almacenes_Graphics.jTextFieldMovil, Gestion_De_Almacenes_Graphics.jTextFieldNIFLetra, Gestion_De_Almacenes_Graphics.jTextFieldNIFNumeros, Gestion_De_Almacenes_Graphics.jTextFieldNombre, Gestion_De_Almacenes_Graphics.jTextFieldTelefono, Gestion_De_Almacenes_Graphics.jTextFieldTotalVentas);
			
			agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldCodigo);

		} else {
			for (Integer erroresCometido : erroresCometidos) {
				switch (erroresCometido) {
					case 1:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_1);
						break;
					case 2:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_2);
						break;
					case 3:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_3);
						break;
					case 4:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_4);
						break;
					case 5:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_5);
						break;
					case 6:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_6);
						break;
					case 7:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_7);
						break;
					case 8:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_8);
						break;
					case 9:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_9);
						break;
					case 10:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_10);
						break;
					case 11:
						recopilacionDeErrores.add(mensajeAMostrar.codigo_error_11);
						break;
				}
			}
			if (recopilacionDeErrores.size() == 1) {
				JOptionPane.showMessageDialog(aThis, "Error: \n" + recopilacionDeErrores.get(0), "Error en el Alta",
						JOptionPane.ERROR_MESSAGE);
			} else {
				String restoDeErrores = "";
				for (int i = 1; i < recopilacionDeErrores.size(); i++) {
					restoDeErrores += recopilacionDeErrores.get(i)+"\n";
				}

                //JOptionPane.showMessageDialog(aThis, "Error en: \n" + recopilacionDeErrores.get(0) + "\n" + "Mas detalles de Error: \n" + restoDeErrores, "Error en el Alta",
				//  JOptionPane.ERROR_MESSAGE);
				// JScrollPane jScrollPaneErrores, JTextArea jTextAreaErrores
				
				int seleccion = JOptionPane.showOptionDialog(
						aThis,
						"Error en: \n" + recopilacionDeErrores.get(0) + "\n Seleccione opción:",
						"Error en Algún Campo",
						JOptionPane.YES_NO_CANCEL_OPTION,
						JOptionPane.QUESTION_MESSAGE,
						null,
						new Object[]{"Volver", "Mas Detalles..."}, // null para YES, NO y CANCEL
						"opcion 1");

				if (seleccion == 0) {
					centrarFocoEnPrimerError(erroresCometidos);
				}

				if (seleccion == 1) {
					
					jPanelMostrarErrores.setVisible(true);

					jTextAreaErrores.setText("Mas detalles de Error:\n\n" + restoDeErrores);

					JOptionPane.showConfirmDialog(null, jPanelMostrarErrores, "Errores Cometidos", JOptionPane.OK_CANCEL_OPTION, JOptionPane.PLAIN_MESSAGE);
					
					centrarFocoEnPrimerError(erroresCometidos);
	
					
				}
			}
		}
	}

	/**
	 * Este método se ejecuta tras mostrar el Mensaje con todos los errores, y
	 * en base al primer código de error almacenado, tras cerrar el JOptionPanel
	 * que nos muestra los errores, procede a colocar el foco sobre el primer
	 * error y limpiar la información existente en el campo que se ha rellenado
	 * de forma errónea, se le pasa como parámetro el arrayList de Enteros que
	 * contiene los códigos de error que se han producido.
	 *
	 * @param erroresCometidos
	 */
	private void centrarFocoEnPrimerError(ArrayList<Integer> erroresCometidos) {
		int campoFoco = erroresCometidos.get(0);
		switch (campoFoco) {
			case 1:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldCodigo);
				break;
			case 2:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldNIFNumeros);
				break;
			case 3:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldNombre);
				break;
			case 4:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldApellidos);
				break;
			case 5:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldDomicilio);
				break;
			case 6:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldCodigoPostal);
				break;
			case 7:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldLocalidad);
				break;
			case 8:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldTelefono);
				break;
			case 9:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldMovil);
				break;
			case 10:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldFax);
				break;
			case 11:
				agarrarFoco(Gestion_De_Almacenes_Graphics.jTextFieldEmail);
				break;
		}
	}

	/**
	 * Este método es un método complementario del método
	 * centrarFocoEnPrimerError Y lo que le pasamos como parámetro es un
	 * JtextField y en base a ese JtextField colocamos el Focus sobre el y
	 * limpiamos el contenido que pudiese estar almacenando hasta ese momento.
	 *
	 * @param jTextField
	 */
	private void agarrarFoco(JTextField jTextField) {
		jTextField.grabFocus();
	}

	/**
	 * Este método se encarga de limitar la entrada de datos desde el Listener
	 * puesto en el Textfield desde el cual lo llamemos, se realiza la llamada
	 * desde un KeyTyped Listener y se le pasa como parámetros el Limite que
	 * queramos y el Evento, con el cual luego podremos con el método Consume
	 * limitar el tamaño de la cadena introducida.
	 *
	 * @param limite
	 * @param evt
	 */
	public void limitadorNumeroCaracteresIntroducidos(int limite, KeyEvent evt, JTextField jTextFieldALimitar) {
		if (jTextFieldALimitar.getText().length() == limite) {
			evt.consume();
		}
	}

	/**
	 * Este método es el encargado de cancelar la Introducción de los Datos que
	 * se hayan rellenado, colocando todos los Campos JTextField a NULL Se le
	 * pasa como parámetros de entrada todos los JTextfield del formulario
	 * Gestion_De_Almacenes_Graphics
	 *
	 * @param jTextFieldApellidos
	 * @param jTextFieldCodigo
	 * @param jTextFieldCodigoPostal
	 * @param jTextFieldDomicilio
	 * @param jTextFieldEmail
	 * @param jTextFieldFax
	 * @param jTextFieldLocalidad
	 * @param jTextFieldMovil
	 * @param jTextFieldNIFLetra
	 * @param jTextFieldNIFNumeros
	 * @param jTextFieldNombre
	 * @param jTextFieldTelefono
	 * @param jTextFieldTotalVentas
	 */
	public void cancelarIntroduccionDatos(JTextField jTextFieldApellidos, JTextField jTextFieldCodigo, JTextField jTextFieldCodigoPostal, JTextField jTextFieldDomicilio, JTextField jTextFieldEmail, JTextField jTextFieldFax, JTextField jTextFieldLocalidad, JTextField jTextFieldMovil, JTextField jTextFieldNIFLetra, JTextField jTextFieldNIFNumeros, JTextField jTextFieldNombre, JTextField jTextFieldTelefono, JTextField jTextFieldTotalVentas) {
		jTextFieldApellidos.setText(null);
		jTextFieldCodigo.setText(null);
		jTextFieldCodigoPostal.setText(null);
		jTextFieldDomicilio.setText(null);
		jTextFieldEmail.setText(null);
		jTextFieldFax.setText(null);
		jTextFieldLocalidad.setText(null);
		jTextFieldMovil.setText(null);
		jTextFieldNIFLetra.setText(null);
		jTextFieldNIFNumeros.setText(null);
		jTextFieldNombre.setText(null);
		jTextFieldTelefono.setText(null);
		jTextFieldTotalVentas.setText(null);
		jTextFieldCodigo.grabFocus();
	}

	/**
	 * Éste método se encarga de rellenar con ceros a la izquierda el Código en
	 * caso de que la cadena tenga un tamaño menor de 6, se ejecuta cuando el
	 * foco del campo Código cambia.
	 *
	 * @param jTextFieldCodigo
	 */
	public void rellenarConCerosAlaIzquierda(JTextField jTextFieldCodigo) {
		String codigo = jTextFieldCodigo.getText().trim();
		if (codigo.length() != 0) {
			while (codigo.length() < 6) {
				codigo = "0" + codigo;
			}
			jTextFieldCodigo.setText(codigo);
		}
	}

	/**
	 * Este método es el encargado de Generar la Letra correspondiente a una
	 * cadena de ocho números, la cual codifica el DNI, se le pasa como
	 * parámetros el jTextField de Números y Letras, con el de Números se
	 * calcula la Letra que le corresponde a esa cadena de 8 números y después
	 * pintamos el resultado sobre el jTextField Letras En el caso de que los
	 * datos introducidos en el JTextField Números, no fueran íntegramente
	 * números saltaría una NumberFormatExcepcion que pararía la ejecución de
	 * este método posteriormente en la validación se comprobaría si NIF números
	 * contiene lo que debe y en base a eso se Informa al Usuario. Cabe
	 * mencionar que este método se ejecuta cuando el foco sobre NIFNumeros se
	 * pierde.
	 *
	 * @param jTextFieldNifNumeros
	 * @param jTextFieldNIFLetra
	 */
	public void generarLetraNIF(JTextField jTextFieldNifNumeros, JTextField jTextFieldNIFLetra) {
		try {
			if (jTextFieldNifNumeros.getText().trim().length() == 8) {
				String conjuntoLetras = "TRWAGMYFPDXBNJZSQVHLCKE";
				int posicionLetra = Integer.parseInt(jTextFieldNifNumeros.getText()) % 23;
				char letraNIF = conjuntoLetras.charAt(posicionLetra);
				jTextFieldNIFLetra.setText(String.valueOf(letraNIF));
			}
		} catch (NumberFormatException e) {
		}
	}

	/**
	 * Este Metodo se encarga de pasar el foco entre campos, procede a comprobar
	 * si la tecla introducida es la tecla Intro y si es asi, pasa el foco al
	 * campo que le indiquemos, se le pasa como parametros el Evento del
	 * KeyTyped y el campo al que queremos asignar el foco tras la pulsacion de
	 * Enter.
	 *
	 * @param evt
	 * @param jTextFieldSiguienteFoco
	 */
	public void pasarFocoSiguienteCampo(KeyEvent evt, JComponent jTextFieldSiguienteFoco) {
		if ((int) evt.getKeyChar() == 10) {
			jTextFieldSiguienteFoco.grabFocus();
		}

	}
	//FIN MÉTODOS CREADOS POR EL DESARROLLADOR    

}
