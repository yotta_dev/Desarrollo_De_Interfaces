/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package b_gestion_De_Almacenes;

/**
 * Voy a utilizar esta clase para almacenar todos los Códigos de Error Posibles
 * que tiene el Programa.
 * 
 * @author Yehoshua
 */
public class Codigos_De_Error {  
    //Error en campo Codigo
    String codigo_error_1="Código: Solo admite Números y Letras - Max: 6 Cáracteres.\n";
    //Error en campo NIF
    String codigo_error_2="NIF: Solo admite Números - 8 Digitos.\n";
    //Error en campo Nombre
    String codigo_error_3="Nombre: Solo admite letras,acentos y puntos. La primera Letra debe ser Mayúscula - Max: 30 Cáracteres.\n";
    //Error en campo Apellidos
    String codigo_error_4="Apellidos: Solo admite letras,acentos,puntos y Guiones. La primera Letra debe ser Mayúscula - Max: 30 Cáracteres.\n";
    //Error en campo Domicilio
    String codigo_error_5="Domicilio: El formato de Domicilio debe ser (P.ej: C/Real º30 2ª) - Max: 35 Cáracteres.\n";
    //Error en campo COdigo Postal
    String codigo_error_6="Código Postal: Solo admite números - 5 Dígitos.\n";
    //Error en campo Localidad
    String codigo_error_7="Localidad: Solo admite letras,acentos,puntos y Guiones. La primera Letra debe ser Mayúscula- Max: 30 Cáracteres.\n";
    //Error en campo Telefono
    String codigo_error_8="Telefono: Solo admite números - 9 Dígitos.\n";
    //Error en campo Movil
    String codigo_error_9="Móvil: Solo admite números - 9 Dígitos.\n";
    //Error en campo Fax
    String codigo_error_10="Fax: Solo admite números - 9 Dígitos.\n";
    //Error en campo Email
    String codigo_error_11="Email: El formato del Email debe ser el siguiente P.ej: (testmail@domain.com)\n          Admite puntos,barrabaja y guion medio.\n";
}
