/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i_1_gestion_De_Almacenes_v1;

import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import static javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE;

/**
 *
 * @author Yehoshua
 */
public class C_Operating_Methods {

    //COMIENZO DE VARIABLES DECLARADAS POR EL DESARROLLADOR
    F_Error_Codes mensajeAMostrar = new F_Error_Codes();
    //FIN DE VARIABLES DECLARADAS POR EL DESARROLLADOR

    //COMIENZO MÉTODOS CREADOS POR EL DESARROLLADOR
    public ArrayList<Integer> validarDatosIntroducidos(JTextField jTextFieldApellidos, JTextField jTextFieldCodigo, JTextField jTextFieldCodigoPostal, JTextField jTextFieldDomicilio, JTextField jTextFieldEmail, JTextField jTextFieldFax, JTextField jTextFieldLocalidad, JTextField jTextFieldMovil, JTextField jTextFieldNIFLetra, JTextField jTextFieldNIFNumeros, JTextField jTextFieldNombre, JTextField jTextFieldTelefono, JTextField jTextFieldTotalVentas) {

        ArrayList<Integer> erroresCometidos = new ArrayList<>();

        if (!jTextFieldCodigo.getText().matches("[0-9]{6}")) {
            erroresCometidos.add(1);
        }
        if (!jTextFieldNIFNumeros.getText().matches("[\\d]{8}")) {
            erroresCometidos.add(2);
        }
        if (!jTextFieldNombre.getText().matches("([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú.]+[\\s]*)+$")) {
            erroresCometidos.add(3);
        }
        if (!jTextFieldApellidos.getText().matches("([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú.-]+[\\s]*)+$")) {
            erroresCometidos.add(4);
        }
        if (!jTextFieldDomicilio.getText().matches("[A-Z][A-z0-9ªºñÑáéíóúÁÉÍÓÚ/\\.,;: -]+{1,30}")) {
            erroresCometidos.add(5);
        }
        if (!jTextFieldCodigoPostal.getText().matches("[\\d]{5}")) {
            erroresCometidos.add(6);
        }
        if (!jTextFieldLocalidad.getText().matches("[A-Z][A-zñÑáéíóúÁÉÍÓÚ., -]+{1,30}")) {
            erroresCometidos.add(7);
        }
        //OPCIONALES
        if (!jTextFieldTelefono.getText().matches("[0-9]{1}([\\d]{2}[-]*){3}[\\d]{2}$") && !jTextFieldTelefono.getText().matches("")) {

            erroresCometidos.add(8);

        }
        if (!jTextFieldMovil.getText().matches("[0-9]{1}([\\d]{2}[-]*){3}[\\d]{2}$") && !jTextFieldMovil.getText().matches("")) {

            erroresCometidos.add(9);

        }
        if (!jTextFieldFax.getText().matches("[0-9]{1}([\\d]{2}[-]*){3}[\\d]{2}$") && !jTextFieldFax.getText().matches("")) {

            erroresCometidos.add(10);

        }
        if (!jTextFieldEmail.getText().toLowerCase().matches("[A-Za-z0-9.-]{1,17}[@][a-z0-9-]{1,17}[.][a-z0-9_-]{1,6}[.]?[a-z0-9_-]{0,6}") && !jTextFieldEmail.getText().matches("")) {

            erroresCometidos.add(11);

        }
        //OPCIONALES

        if (erroresCometidos.isEmpty()) {

            jTextFieldTotalVentas.setText("0");
            agarrarFoco(jTextFieldCodigo);
        }
        return erroresCometidos;
    }

    public ArrayList<Integer> validarDatosIntroducidos_Pedidos(JTextField jTextFieldCodigo, JTextField jTextFieldApellidos, JTextField jTextFieldCodigoPostal, JTextField jTextFieldDomicilio, JTextField jTextFieldLocalidad, JTextField jTextFieldNIFLetra, JTextField jTextFieldNIFNumeros, JTextField jTextFieldNombre, JTextField jTextFieldTotalVentas) {
        ArrayList<Integer> erroresCometidos = new ArrayList<>();

        if (!jTextFieldCodigo.getText().matches("[0-9]{6}")) {
            erroresCometidos.add(1);
        }
        if (!jTextFieldNIFNumeros.getText().matches("[\\d]{8}")) {
            erroresCometidos.add(2);
        }
        if (!jTextFieldNombre.getText().matches("([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú.]+[\\s]*)+$")) {
            erroresCometidos.add(3);
        }
        if (!jTextFieldApellidos.getText().matches("([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú.-]+[\\s]*)+$")) {
            erroresCometidos.add(4);
        }
        if (!jTextFieldDomicilio.getText().matches("[A-Z][A-z0-9ªºñÑáéíóúÁÉÍÓÚ/\\.,;: -]+{1,30}")) {
            erroresCometidos.add(5);
        }
        if (!jTextFieldCodigoPostal.getText().matches("[\\d]{5}")) {
            erroresCometidos.add(6);
        }
        if (!jTextFieldLocalidad.getText().matches("[A-Z][A-zñÑáéíóúÁÉÍÓÚ., -]+{1,30}")) {
            erroresCometidos.add(7);
        }
        //OPCIONALES

        if (erroresCometidos.isEmpty()) {

            jTextFieldTotalVentas.setText("0");
            agarrarFoco(jTextFieldCodigo);
        }
        return erroresCometidos;
    }

    /**
     * Éste método se apoya en el Método que valida los Datos para construir en
     * un String toda la cadena de errores Cometidos, se le pasa como parámetro
     * un arraylist de enteros, que almacena los códigos de error cometidos, y
     * en base a cada código de error, se almacena en la cadena de recopilación
     * de errores y posteriormente se muestra como error.código de error en
     * forma de String
     *
     * @param thisGUI
     * @param erroresCometidos
     * @return
     *
     */
    public boolean comprobacionDeErrores(B_0_Graphic_User_Interface_Clientes thisGUI, ArrayList<Integer> erroresCometidos) {

        ArrayList<String> recopilacionDeErrores = new ArrayList<>();

        if (erroresCometidos.isEmpty()) {

            return true;

        } else {

            for (Integer erroresCometido : erroresCometidos) {
                switch (erroresCometido) {
                    case 1:
                        recopilacionDeErrores.add(mensajeAMostrar.codigo_error_1);
                        break;
                    case 2:
                        recopilacionDeErrores.add(mensajeAMostrar.codigo_error_2);
                        break;
                    case 3:
                        recopilacionDeErrores.add(mensajeAMostrar.codigo_error_3);
                        break;
                    case 4:
                        recopilacionDeErrores.add(mensajeAMostrar.codigo_error_4);
                        break;
                    case 5:
                        recopilacionDeErrores.add(mensajeAMostrar.codigo_error_5);
                        break;
                    case 6:
                        recopilacionDeErrores.add(mensajeAMostrar.codigo_error_6);
                        break;
                    case 7:
                        recopilacionDeErrores.add(mensajeAMostrar.codigo_error_7);
                        break;
                }
            }

            if (recopilacionDeErrores.size() == 1) {

                JOptionPane.showMessageDialog(thisGUI, "Error: \n" + recopilacionDeErrores.get(0), "Error en el Alta",
                        JOptionPane.ERROR_MESSAGE);
                centrarFocoEnPrimerError(erroresCometidos);

            } else {

                String restoDeErrores = "";

                for (int i = 1; i < recopilacionDeErrores.size(); i++) {
                    restoDeErrores += recopilacionDeErrores.get(i) + "\n";
                }

                //JOptionPane.showMessageDialog(aThis, "Error en: \n" + recopilacionDeErrores.get(0) + "\n" + "Mas detalles de Error: \n" + restoDeErrores, "Error en el Alta",
                //  JOptionPane.ERROR_MESSAGE);
                // JScrollPane jScrollPaneErrores, JTextArea jTextAreaErrores
                int seleccion = JOptionPane.showOptionDialog(thisGUI,
                        "Error en: \n" + recopilacionDeErrores.get(0) + "\n Seleccione opción:",
                        "Campo Mal Introducido",
                        JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        new Object[]{"Volver", "Más Detalles..."}, // null para YES, NO y CANCEL
                        "opcion 1");

                if (seleccion == 0) {
                    centrarFocoEnPrimerError(erroresCometidos);
                }

                if (seleccion == 1) {

                    thisGUI.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

                    thisGUI.setEnabled(false);

                    G_Mas_detalles mas_detalles = new G_Mas_detalles(thisGUI);
                    mas_detalles.setVisible(true);
                    mas_detalles.jTextAreaErrores.setText("Más detalles de Error:\n\n" + restoDeErrores);

                    centrarFocoEnPrimerError(erroresCometidos);
                }
            }

            return false;

        }
    }

    /**
     * Este método se ejecuta tras mostrar el Mensaje con todos los errores, y
     * en base al primer código de error almacenado, tras cerrar el JOptionPanel
     * que nos muestra los errores, procede a colocar el foco sobre el primer
     * error y limpiar la información existente en el campo que se ha rellenado
     * de forma errónea, se le pasa como parámetro el arrayList de Enteros que
     * contiene los códigos de error que se han producido.
     *
     * @param erroresCometidos
     */
    public void centrarFocoEnPrimerError(ArrayList<Integer> erroresCometidos) {
        int campoFoco = erroresCometidos.get(0);
        switch (campoFoco) {
            case 1:
                B_0_Graphic_User_Interface_Clientes.jTextFieldCodigo.setEnabled(true);
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldCodigo);
                break;
            case 2:
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldNIFNumeros);
                break;
            case 3:
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldNombre);
                break;
            case 4:
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldApellidos);
                break;
            case 5:
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldDomicilio);
                break;
            case 6:
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldCodigoPostal);
                break;
            case 7:
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldLocalidad);
                break;
            case 8:
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldTelefono);
                break;
            case 9:
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldMovil);
                break;
            case 10:
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldFax);
                break;
            case 11:
                agarrarFoco(B_0_Graphic_User_Interface_Clientes.jTextFieldEmail);
                break;
        }
    }

    /**
     * Este método es un método complementario del método
     * centrarFocoEnPrimerError Y lo que le pasamos como parámetro es un
     * JtextField y en base a ese JtextField colocamos el Focus sobre el y
     * limpiamos el contenido que pudiese estar almacenando hasta ese momento.
     *
     * @param jTextField
     */
    private void agarrarFoco(JTextField jTextField) {
        jTextField.grabFocus();
    }

    /**
     * Este método se encarga de limitar la entrada de datos desde el Listener
     * puesto en el Textfield desde el cual lo llamemos, se realiza la llamada
     * desde un KeyTyped Listener y se le pasa como parámetros el Limite que
     * queramos y el Evento, con el cual luego podremos con el método Consume
     * limitar el tamaño de la cadena introducida.
     *
     * @param limite
     * @param evt
     * @param jTextFieldALimitar
     */
    public void limitadorNumeroCaracteresIntroducidos(int limite, KeyEvent evt, JTextField jTextFieldALimitar) {
        if (jTextFieldALimitar.getText().length() == limite) {
            evt.consume();
        }
    }

    /**
     * Este método es el encargado de cancelar la Introducción de los Datos que
     * se hayan rellenado, colocando todos los Campos JTextField a NULL Se le
     * pasa como parámetros de entrada todos los JTextfield del formulario
     * Gestion_De_Almacenes_Graphics
     *
     * @param jButtonAceptar
     * @param jButtonCancelar
     * @param jTextFieldApellidos
     * @param jTextFieldCodigo
     * @param jTextFieldCodigoPostal
     * @param jTextFieldDomicilio
     * @param jTextFieldEmail
     * @param jTextFieldFax
     * @param jTextFieldLocalidad
     * @param jTextFieldMovil
     * @param jTextFieldNIFLetra
     * @param jTextFieldNIFNumeros
     * @param jTextFieldNombre
     * @param jTextFieldTelefono
     * @param jTextFieldTotalVentas
     */
    public void cancelarIntroduccionDatos(JButton jButtonAceptar, JButton jButtonCancelar, JTextField jTextFieldCodigo, JTextField jTextFieldNIFNumeros, JTextField jTextFieldNIFLetra, JTextField jTextFieldApellidos, JTextField jTextFieldNombre, JTextField jTextFieldDomicilio, JTextField jTextFieldCodigoPostal, JTextField jTextFieldLocalidad, JTextField jTextFieldTelefono, JTextField jTextFieldMovil, JTextField jTextFieldFax, JTextField jTextFieldEmail, JTextField jTextFieldTotalVentas) {

        jButtonAceptar.setEnabled(false);

        jButtonCancelar.setEnabled(false);

        jTextFieldCodigo.setText(null);
        jTextFieldCodigo.grabFocus();
        jTextFieldCodigo.setEnabled(true);

        jTextFieldNIFNumeros.setText(null);
        jTextFieldNIFNumeros.setEnabled(false);

        jTextFieldNIFLetra.setText(null);

        jTextFieldApellidos.setText(null);
        jTextFieldApellidos.setEnabled(false);

        jTextFieldNombre.setText(null);
        jTextFieldNombre.setEnabled(false);

        jTextFieldDomicilio.setText(null);
        jTextFieldDomicilio.setEnabled(false);

        jTextFieldCodigoPostal.setText(null);
        jTextFieldCodigoPostal.setEnabled(false);

        jTextFieldLocalidad.setText(null);
        jTextFieldLocalidad.setEnabled(false);

        jTextFieldTelefono.setText(null);
        jTextFieldTelefono.setEnabled(false);

        jTextFieldMovil.setText(null);
        jTextFieldMovil.setEnabled(false);

        jTextFieldFax.setText(null);
        jTextFieldFax.setEnabled(false);

        jTextFieldEmail.setText(null);
        jTextFieldEmail.setEnabled(false);

        jTextFieldTotalVentas.setText(null);
    }

    public void volver(JButton jButtonAceptar, JButton jButtonCancelar, JTextField jTextFieldApellidos, JTextField jTextFieldCodigo, JTextField jTextFieldCodigoPostal, JTextField jTextFieldDomicilio, JTextField jTextFieldEmail, JTextField jTextFieldFax, JTextField jTextFieldLocalidad, JTextField jTextFieldMovil, JTextField jTextFieldNIFLetra, JTextField jTextFieldNIFNumeros, JTextField jTextFieldNombre, JTextField jTextFieldTelefono, JTextField jTextFieldTotalVentas) {
        jButtonAceptar.setEnabled(false);
        jButtonCancelar.setEnabled(false);

        jTextFieldCodigo.setText(null);
        jTextFieldCodigo.setEnabled(false);

        jTextFieldApellidos.setText(null);
        jTextFieldApellidos.setEnabled(false);

        jTextFieldCodigoPostal.setText(null);
        jTextFieldCodigoPostal.setEnabled(false);

        jTextFieldDomicilio.setText(null);
        jTextFieldDomicilio.setEnabled(false);

        jTextFieldEmail.setText(null);
        jTextFieldEmail.setEnabled(false);

        jTextFieldFax.setText(null);
        jTextFieldFax.setEnabled(false);

        jTextFieldLocalidad.setText(null);
        jTextFieldLocalidad.setEnabled(false);

        jTextFieldMovil.setText(null);
        jTextFieldMovil.setEnabled(false);

        jTextFieldNIFLetra.setText(null);

        jTextFieldNIFNumeros.setText(null);
        jTextFieldNIFNumeros.setEnabled(false);

        jTextFieldNombre.setText(null);
        jTextFieldNombre.setEnabled(false);

        jTextFieldTelefono.setText(null);
        jTextFieldTelefono.setEnabled(false);

        jTextFieldTotalVentas.setText(null);

    }

    /**
     * Éste método se encarga de rellenar con ceros a la izquierda el Código en
     * caso de que la cadena tenga un tamaño menor de 6, se ejecuta cuando el
     * foco del campo Código cambia.
     *
     * @param jTextFieldCodigo
     */
    public void rellenarConCerosAlaIzquierda(JTextField jTextFieldCodigo) {

        String codigo = jTextFieldCodigo.getText().trim();

        if (codigo.length() != 0) {
            while (codigo.length() < 6) {
                codigo = "0" + codigo;
            }
            jTextFieldCodigo.setText(codigo);
        } else {
            codigo = "000000";
            jTextFieldCodigo.setText(codigo);
        }
    }

    /**
     * Este método es el encargado de Generar la Letra correspondiente a una
     * cadena de ocho números, la cual codifica el DNI, se le pasa como
     * parámetros el jTextField de Números y Letras, con el de Números se
     * calcula la Letra que le corresponde a esa cadena de 8 números y después
     * pintamos el resultado sobre el jTextField Letras En el caso de que los
     * datos introducidos en el JTextField Números, no fueran íntegramente
     * números saltaría una NumberFormatExcepcion que pararía la ejecución de
     * este método posteriormente en la validación se comprobaría si NIF números
     * contiene lo que debe y en base a eso se Informa al Usuario. Cabe
     * mencionar que este método se ejecuta cuando el foco sobre NIFNumeros se
     * pierde.
     *
     * @param jTextFieldNifNumeros
     * @param jTextFieldNIFLetra
     */
    public void generarLetraNIF(JTextField jTextFieldNifNumeros, JTextField jTextFieldNIFLetra) {
        try {
            if (jTextFieldNifNumeros.getText().trim().length() == 8) {
                String conjuntoLetras = "TRWAGMYFPDXBNJZSQVHLCKE";
                int posicionLetra = Integer.parseInt(jTextFieldNifNumeros.getText()) % 23;
                char letraNIF = conjuntoLetras.charAt(posicionLetra);
                jTextFieldNIFLetra.setText(String.valueOf(letraNIF));
            }
        } catch (NumberFormatException e) {
        }
    }

    /**
     * Este Metodo se encarga de pasar el foco entre campos, procede a comprobar
     * si la tecla introducida es la tecla Intro y si es asi, pasa el foco al
     * campo que le indiquemos, se le pasa como parametros el Evento del
     * KeyTyped y el campo al que queremos asignar el foco tras la pulsacion de
     * Enter.
     *
     * @param evt
     * @param jTextFieldSiguienteFoco
     */
    public void pasarFocoSiguienteCampo(KeyEvent evt, JComponent jcomponent) {

        if ((int) evt.getKeyChar() == 10) {
            jcomponent.setEnabled(true);
            jcomponent.grabFocus();
        }

    }

    public void mostrarMensajeError(String mensaje, String cabecera) {

        JOptionPane.showMessageDialog(null, mensaje, cabecera, JOptionPane.ERROR_MESSAGE);

    }

    public void mostrarMensajeErrorContexto(javax.swing.JFrame padre, String mensaje, String cabecera) {

        JOptionPane.showMessageDialog(padre, mensaje, cabecera, JOptionPane.ERROR_MESSAGE);

    }

    public boolean mostrarMensaje_Si_o_No(String mensaje, String cabecera) {

        if (JOptionPane.showConfirmDialog(null, mensaje, cabecera,
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            return true;
        } else {
            return false;
        }
    }
    //FIN MÉTODOS CREADOS POR EL DESARROLLADOR    

    void habilitarEdicion(boolean activador, JButton jButtonAceptar, JButton jButtonCancelar, JTextField jTextFieldCodigo, JTextField jTextFieldNIFNumeros, JTextField jTextFieldNIFLetra, JTextField jTextFieldApellidos, JTextField jTextFieldNombre, JTextField jTextFieldDomicilio, JTextField jTextFieldCodigoPostal, JTextField jTextFieldLocalidad, JTextField jTextFieldTelefono, JTextField jTextFieldMovil, JTextField jTextFieldFax, JTextField jTextFieldEmail, JTextField jTextFieldTotalVentas) {

        jButtonAceptar.setEnabled(activador);

        jButtonCancelar.setEnabled(activador);

        jTextFieldCodigo.setEnabled(false);
        jTextFieldNIFNumeros.setEnabled(false);
        jTextFieldNIFLetra.setEnabled(false);

        jTextFieldApellidos.setEnabled(activador);

        jTextFieldNombre.setEnabled(activador);

        jTextFieldDomicilio.setEnabled(activador);

        jTextFieldCodigoPostal.setEnabled(activador);

        jTextFieldLocalidad.setEnabled(activador);

        jTextFieldTelefono.setEnabled(activador);

        jTextFieldMovil.setEnabled(activador);

        jTextFieldFax.setEnabled(activador);

        jTextFieldEmail.setEnabled(activador);

    }

    public boolean compararObjectosCliente(H_Cliente cliente1, H_Cliente cliente2) {

        String totalVentas1 = String.valueOf(cliente1.getTotal_ventas());
        String totalVentas2 = String.valueOf(cliente2.getTotal_ventas());

        if (cliente1.getCodigo().equals(cliente2.getCodigo())
                && cliente1.getNif().equals(cliente2.getNif())
                && cliente1.getNombre().equals(cliente2.getNombre())
                && cliente1.getApellidos().equals(cliente2.getApellidos())
                && cliente1.getDomicilio().equals(cliente2.getDomicilio())
                && cliente1.getCodigo_postal().equals(cliente2.getCodigo_postal())
                && cliente1.getLocalidad().equals(cliente2.getLocalidad())
                && cliente1.getTelefono().equals(cliente2.getTelefono())
                && cliente1.getMovil().equals(cliente2.getMovil())
                && cliente1.getFax().equals(cliente2.getFax())
                && cliente1.getEmail().equals(cliente2.getEmail())
                && totalVentas1.equals(totalVentas2)) {

            return true;

        } else {

            return false;

        }
    }

    public void volver_Pedidos(JButton jButtonAceptar, JButton jButtonCancelar, JTextField jTextFieldApellidos, JTextField jTextFieldCodigo, JTextField jTextFieldCodigoPostal, JTextField jTextFieldDomicilio, JTextField jTextFieldLocalidad, JTextField jTextFieldNIFLetra, JTextField jTextFieldNIFNumeros, JTextField jTextFieldNombre, JTextField jTextFieldTotalVentas) {
        jButtonAceptar.setEnabled(false);
        jButtonCancelar.setEnabled(false);

        jTextFieldCodigo.setText(null);
        jTextFieldCodigo.setEnabled(false);

        jTextFieldApellidos.setText(null);
        jTextFieldApellidos.setEnabled(false);

        jTextFieldCodigoPostal.setText(null);
        jTextFieldCodigoPostal.setEnabled(false);

        jTextFieldDomicilio.setText(null);
        jTextFieldDomicilio.setEnabled(false);

        jTextFieldLocalidad.setText(null);
        jTextFieldLocalidad.setEnabled(false);

        jTextFieldNIFLetra.setText(null);

        jTextFieldNIFNumeros.setText(null);
        jTextFieldNIFNumeros.setEnabled(false);

        jTextFieldNombre.setText(null);
        jTextFieldNombre.setEnabled(false);

        jTextFieldTotalVentas.setText(null);
    }

    public void habilitarEdicion_Pedidos(boolean activador, JButton jButtonAceptar, JButton jButtonCancelar, JTextField jTextFieldCodigo, JTextField jTextFieldNIFNumeros, JTextField jTextFieldNIFLetra, JTextField jTextFieldApellidos, JTextField jTextFieldNombre, JTextField jTextFieldDomicilio, JTextField jTextFieldCodigoPostal, JTextField jTextFieldLocalidad, JTextField jTextFieldTotalVentas) {
        jButtonAceptar.setEnabled(activador);

        jButtonCancelar.setEnabled(activador);

        jTextFieldCodigo.setEnabled(false);
        jTextFieldNIFNumeros.setEnabled(false);
        jTextFieldNIFLetra.setEnabled(false);

        jTextFieldApellidos.setEnabled(activador);

        jTextFieldNombre.setEnabled(activador);

        jTextFieldDomicilio.setEnabled(activador);

        jTextFieldCodigoPostal.setEnabled(activador);

        jTextFieldLocalidad.setEnabled(activador);

    }
}
