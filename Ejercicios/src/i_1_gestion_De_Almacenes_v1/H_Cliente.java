/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i_1_gestion_De_Almacenes_v1;

/**
 *
 * @author alumno
 */
public class H_Cliente {
	
	String codigo;
	String nif;
	String apellidos;
	String nombre;
	String domicilio;
	String codigo_postal;
	String localidad;
	String telefono;
	String movil;
	String fax;
	String email;
	float total_ventas;

	public H_Cliente(String codigo, String nif, String apellidos, String nombre, String domicilio, String codigo_postal, String localidad, String telefono, String movil, String fax, String email, float total_ventas) {
		this.codigo = codigo;
		this.nif = nif;
		this.apellidos = apellidos;
		this.nombre = nombre;
		this.domicilio = domicilio;
		this.codigo_postal = codigo_postal;
		this.localidad = localidad;
		this.telefono = telefono;
		this.movil = movil;
		this.fax = fax;
		this.email = email;
		this.total_ventas = total_ventas;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getCodigo_postal() {
		return codigo_postal;
	}

	public void setCodigo_postal(String codigo_postal) {
		this.codigo_postal = codigo_postal;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getMovil() {
		return movil;
	}

	public void setMovil(String movil) {
		this.movil = movil;
	}

	public String getFax() {
		return fax;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public float getTotal_ventas() {
		return total_ventas;
	}

	public void setTotal_ventas(float total_ventas) {
		this.total_ventas = total_ventas;
	}
	
}
