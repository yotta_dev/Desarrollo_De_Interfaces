/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i_1_gestion_De_Almacenes_v1;


/**
 *
 * @author TheSlayeOne1
 */
public class J_Pedido {
    
    String id_cliente;    
    String id_articulo;
    int unidades;
    int precio;
    int importe;

    public J_Pedido(String id_cliente, String id_articulo, int unidades, int precio, int importe) {
        this.id_cliente = id_cliente;
        this.id_articulo = id_articulo;
        this.unidades = unidades;
        this.precio = precio;
        this.importe = importe;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getId_articulo() {
        return id_articulo;
    }

    public void setId_articulo(String id_articulo) {
        this.id_articulo = id_articulo;
    }

    public int getUnidades() {
        return unidades;
    }

    public void setUnidades(int unidades) {
        this.unidades = unidades;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public int getImporte() {
        return importe;
    }

    public void setImporte(int importe) {
        this.importe = importe;
    }
    
    
    
}
