/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i_1_gestion_De_Almacenes_v1;

/**
 * Voy a utilizar esta clase para almacenar todos los Códigos de Error Posibles
 * que tiene el Programa.
 * 
 * @author Yehoshua
 */
public class F_Error_Codes {  
    //Error en campo Codigo
    String codigo_error_1="Código: Solo admite Números y Letras - Máx: 6 Caracteres.\n";
    //Error en campo NIF
    String codigo_error_2="NIF: Solo admite Números - 8 Digitos.\n";
    //Error en campo Nombre
    String codigo_error_3="Nombre: Solo admite letras,acentos y puntos. La primera Letra debe ser Mayúscula - Máx: 30 Caracteres.\n";
    //Error en campo Apellidos
    String codigo_error_4="Apellidos: Solo admite letras,acentos,puntos y Guiones. La primera Letra debe ser Mayúscula - Máx: 30 Caracteres.\n";
    //Error en campo Domicilio
    String codigo_error_5="Domicilio: El formato de Domicilio debe ser (P.ej: C/Real º30 2ª) - Máx: 35 Caracteres.\n";
    //Error en campo Codigo Postal
    String codigo_error_6="Código Postal: Solo admite números - 5 Dígitos.\n";
    //Error en campo Localidad
    String codigo_error_7="Localidad: Solo admite letras,acentos,puntos y Guiones. La primera Letra debe ser Mayúscula- Máx: 30 Caracteres.\n";    
}
