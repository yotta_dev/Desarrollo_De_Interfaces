CREATE DATABASE ALMACEN;

USE ALMACEN;

CREATE TABLE ClientesAltas (
	codigo VARCHAR(6),
	nif VARCHAR(9),
	apellidos VARCHAR(35),
	nombre VARCHAR(15),
	domicilio VARCHAR(40),
	codigo_postal VARCHAR(5),
	localidad VARCHAR(20),
	telefono VARCHAR(9),
	movil VARCHAR(9),
	fax VARCHAR(9),
	email VARCHAR(20),
	total_ventas FLOAT(5),
	PRIMARY KEY (codigo)
);

CREATE TABLE ProveedoresAltas (
	codigo VARCHAR(6),
	nif VARCHAR(9),
	apellidos VARCHAR(35),
	nombre VARCHAR(15),
	domicilio VARCHAR(40),
	codigo_postal VARCHAR(5),
	localidad VARCHAR(20),
	telefono VARCHAR(9),
	movil VARCHAR(9),
	fax VARCHAR(9),
	email VARCHAR(20),
	total_ventas FLOAT(5),
	PRIMARY KEY (codigo)
);

CREATE TABLE ClientesBajas (
        id_baja INTEGER (10)AUTO_INCREMENT,
	codigo VARCHAR(6),
	nif VARCHAR(9),
	apellidos VARCHAR(35),
	nombre VARCHAR(15),
	domicilio VARCHAR(40),
	codigo_postal VARCHAR(5),
	localidad VARCHAR(20),
	telefono VARCHAR(9),
	movil VARCHAR(9),
	fax VARCHAR(9),
	email VARCHAR(20),
	total_ventas FLOAT(5),
	PRIMARY KEY (id_baja)
);

CREATE TABLE ClientesModificaciones (
        id_modificacion INTEGER (10)AUTO_INCREMENT,
	codigo VARCHAR(6),
	nif VARCHAR(9),
	apellidos VARCHAR(35),
	nombre VARCHAR(15),
	domicilio VARCHAR(40),
	codigo_postal VARCHAR(5),
	localidad VARCHAR(20),
	telefono VARCHAR(9),
	movil VARCHAR(9),
	fax VARCHAR(9),
	email VARCHAR(20),
	total_ventas FLOAT(5),
	PRIMARY KEY (id_modificacion)
);

CREATE TABLE ConsultasPorCodigo (
	codigo VARCHAR(6),
	nif VARCHAR(9),
	apellidos VARCHAR(35),
	nombre VARCHAR(15),
	domicilio VARCHAR(40),
	codigo_postal VARCHAR(5),
	localidad VARCHAR(20),
	telefono VARCHAR(9),
	movil VARCHAR(9),
	fax VARCHAR(9),
	email VARCHAR(20),
	total_ventas FLOAT(5),
	PRIMARY KEY (codigo)
);

UPDATE " + tabla + "SET apellidos=?,nombre=?,domicilio=?,codigo_postal=?,localidad=?,telefono=?,movil=?,fax=?,email=?,total_ventas=? WHERE codigo = ?;

UPDATE ClientesModificaciones SET apellidos=ZZzzzzzzz WHERE codigo=000000;

//Consulta Informe
select codigo,apellidos,nombre,total_ventas from clientesAltas ORDER BY codigo;

C:\Users\TheSlayeOne1\Desktop\Repositorios\Desarrollo_De_Interfaces\Ejercicios\src\h_gestion_De_Almacenes_Avanzado_Reportes

SET autocommit off;

SHOW VARIABLES LIKE 'autocommit';

CREATE TABLE Articulos (
	id_articulo VARCHAR(6),
	descripcion VARCHAR(35),
	stock VARCHAR(15),
	precioCompra VARCHAR(15),
	precioVenta VARCHAR(15),
	PRIMARY KEY (id_articulo)
);

CREATE TABLE PedidosClientes (
	id_pedido INTEGER(6)AUTO_INCREMENT,
	id_cliente VARCHAR(6),
	fecha_pedido DATE,
	id_articulo VARCHAR(6),
	unidades VARCHAR(15),
	precio VARCHAR(15),
	importe VARCHAR(15),
	PRIMARY KEY (id_pedido),
	FOREIGN KEY (id_cliente) REFERENCES ClientesAltas(codigo),
	FOREIGN KEY (id_articulo) REFERENCES Articulos(id_articulo)
);

CREATE TABLE PedidosProveedores (
	id_pedido INTEGER(6)AUTO_INCREMENT,
	id_proveedor VARCHAR(6),
	fecha_pedido DATE,
	id_articulo VARCHAR(6),
	unidades VARCHAR(15),
	precio VARCHAR(15),
	importe VARCHAR(15),
	PRIMARY KEY (id_pedido),
	FOREIGN KEY (id_proveedor) REFERENCES ProveedoresAltas(codigo),
	FOREIGN KEY (id_articulo) REFERENCES Articulos(id_articulo)
);

INSERT INTO ProveedoresAltas(codigo,nif,apellidos,nombre,domicilio,codigo_postal,localidad,telefono,movil,fax,email,total_ventas)
VALUES ('000001','56987231Y','Carrefour','Carrefour','C/Carrefour 45','32569','Carrefour','916897543','698536124','111111111','carrefour@gmail.com',0.0);
INSERT INTO ProveedoresAltas(codigo,nif,apellidos,nombre,domicilio,codigo_postal,localidad,telefono,movil,fax,email,total_ventas)
VALUES ('000002','12345678Y','Mercadona','Mercadona','C/Mercadona 45','32569','Mercadona','916897543','698536124','111111111','Mercadona@gmail.com',0.0);
INSERT INTO ProveedoresAltas(codigo,nif,apellidos,nombre,domicilio,codigo_postal,localidad,telefono,movil,fax,email,total_ventas)
VALUES ('000003','87654321Y','DIA','DIA','C/DIA 45','32569','DIA','916897543','698536124','111111111','DIA@gmail.com',0.0);

Aticulos Antiguos
INSERT INTO Articulos(id_articulo,descripcion,stock,precioCompra,precioVenta)
VALUES ('000001','Articulo_1',5,20,25);
INSERT INTO Articulos(id_articulo,descripcion,stock,precioCompra,precioVenta)
VALUES ('000002','Articulo_2',10,25,30);
INSERT INTO Articulos(id_articulo,descripcion,stock,precioCompra,precioVenta)
VALUES ('000003','Articulo_3',15,15,20);
INSERT INTO Articulos(id_articulo,descripcion,stock,precioCompra,precioVenta)
VALUES ('000004','Articulo_4',20,10,15);
INSERT INTO Articulos(id_articulo,descripcion,stock,precioCompra,precioVenta)
VALUES ('000005','Articulo_5',25,5,10);

ArticulosWeb
INSERT INTO Articulos(id_articulo,descripcion,stock,stockMinimo,precioCompra,precioVenta)
VALUES ('000001','Articulo_1',5,1,20,25);
INSERT INTO Articulos(id_articulo,descripcion,stock,stockMinimo,precioCompra,precioVenta)
VALUES ('000002','Articulo_2',10,2,25,30);
INSERT INTO Articulos(id_articulo,descripcion,stock,stockMinimo,precioCompra,precioVenta)
VALUES ('000003','Articulo_3',15,3,15,20);
INSERT INTO Articulos(id_articulo,descripcion,stock,stockMinimo,precioCompra,precioVenta)
VALUES ('000004','Articulo_4',20,4,10,15);
INSERT INTO Articulos(id_articulo,descripcion,stock,stockMinimo,precioCompra,precioVenta)
VALUES ('000005','Articulo_5',25,5,5,10);


INSERT INTO ClientesAltas(codigo,nif,apellidos,nombre,domicilio,codigo_postal,localidad,telefono,movil,fax,email,total_ventas)
VALUES ('000000','06018454K','Salazar','Yehoshua','C/Real 37','28400','Villalba','918512759','674672616','999999999','test1@gmail.com',0.0);
INSERT INTO ClientesAltas(codigo,nif,apellidos,nombre,domicilio,codigo_postal,localidad,telefono,movil,fax,email,total_ventas)
VALUES ('000001','56987231Y','Michael','Ron','C/Mayor 75','28400','Villalba','918512759','698362124','111111111','ron_michael@gmail.com',0.0);
INSERT INTO ClientesAltas(codigo,nif,apellidos,nombre,domicilio,codigo_postal,localidad,telefono,movil,fax,email,total_ventas)
VALUES ('000002','56987231Y','Banatao','Alba','C/Real 89','28028','Madrid','916897543','698562124','111111111','alba_banatao@gmail.com',0.0);


CREATE TABLE Articulos (
	id_articulo VARCHAR(6),
	descripcion VARCHAR(35),
	stock VARCHAR(15),
	stockMinimo VARCHAR(15),
	precioCompra VARCHAR(15),
	precioVenta VARCHAR(15),
	PRIMARY KEY (id_articulo)
);


CREATE TABLE Historica (
	codigoPoC VARCHAR(15),
	codigoPoCI VARCHAR(15),
	codigo VARCHAR(15),
	unidades FLOAT(5),
	fecha_pedido DATE,
	PRIMARY KEY (codigoPoC)
);

