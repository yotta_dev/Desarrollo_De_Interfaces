/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package i_1_gestion_De_Almacenes_v1;

/**
 *
 * @author TheSlayeOne1
 */
public class I_Articulo {

    String id_articulo;
    String descripcion;
    int stock;
    int precioCompra;
	int precioVenta;

    public I_Articulo(String id_articulo, String descripcion, int stock, int precioCompra,int precioVenta) {
        this.id_articulo = id_articulo;
        this.descripcion = descripcion;
        this.stock = stock;
        this.precioCompra = precioCompra;
		this.precioVenta = precioVenta;
    }

    public String getId_articulo() {
        return id_articulo;
    }

    public void setId_articulo(String id_articulo) {
        this.id_articulo = id_articulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public int getPrecioCompra() {
        return precioCompra;
    }

    public void setPrecioCompra(int precio) {
        this.precioCompra = precio;
    }
    
    public int getPrecioVenta() {
        return precioVenta;
    }

    public void setPrecioVenta(int precioVenta) {
        this.precioVenta = precioVenta;
    }

}
