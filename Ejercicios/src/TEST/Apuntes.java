/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TEST;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author TheSlayeOne1
 */
public class Apuntes {

    static Connection connection = null; // Conexion con la BD

    public static void main(String[] args) {
        //FICHEROS
        System.out.print("Introduzca la ruta del Directorio: ");

        String ruta = Entrada.cadena();

        listarFicheros(ruta);

        //Leer el contenido de un fichero, devuelve el contenido del fichero
        leerContenidoFichero(ruta);
        
        String contenido = null;
        
        guardarContenidoFichero(contenido);

        //BASES DE DATOS        
        try {

            conectarse_A_BBDD(connection);

            select_fila_BBDD("tabla", "clave");

            delete_fila_BBDD("tabla", "clave");

            Prueba prueba = new Prueba("dato1", "dato2", "dato3");

            insert_fila_BBDD("tabla", prueba);

            //Lista el contenido de una tabla en un ArrayList de Objectos del tipo de fila de la tabla
            listar_tabla_toArrayList("tabla");

            desconectarse_de_BBDD(connection);

        } catch (Exception ex) {

            mostrarMensajeError(ex.getMessage(), "Error SQL");

        }
        //FIN BASE DE DATOS

    }

    private static void listarFicheros(String ruta) {
        int reset;

        do {
            reset = 0;

            File directorio = new File(ruta);

            if (directorio.exists()) {

                if (directorio.isDirectory()) {

                    File[] contenidoDirectorio = directorio.listFiles();

                    if (contenidoDirectorio != null) {
                        for (int i = 0; i < contenidoDirectorio.length; i++) {

                            if (contenidoDirectorio[i].isFile()) {
                                System.out.println("-  Fichero   - " + contenidoDirectorio[i].getName());
                            }

                            if (contenidoDirectorio[i].isDirectory()) {
                                System.out.println("- Directorio - " + contenidoDirectorio[i].getName());
                                System.out.print("   -");
                                listarFicheros(contenidoDirectorio[i].getAbsolutePath());
                            }
                            reset = 0;
                        }
                    } else {
                        System.out.println("El Directorio Seleccionado esta Vacio");
                    }
                } else {
                    System.out.print("Introduzca una ruta que corresponda a un directorio: ");
                    ruta = Entrada.cadena();
                    reset = 1;
                }
            } else {
                System.out.print("Introduzca una ruta que Exista: ");
                ruta = Entrada.cadena();
                reset = 1;
            }
        } while (reset == 1);

    }

    public static void conectarse_A_BBDD(Connection connection) {

        try {
            Class.forName("com.mysql.jdbc.Driver");

            String cadenaConexion = "jdbc:mysql://localhost:3306/ALMACEN";
            String usuario = "root";
            String password = "root";

            connection = (com.mysql.jdbc.Connection) DriverManager.getConnection(cadenaConexion, usuario, password);

        } catch (ClassNotFoundException ex) {

            mostrarMensajeError(ex.getMessage(), "Error en Driver MySQL");

        } catch (SQLException ex) {

            mostrarMensajeError(ex.getMessage(), "Error SQL");

        }
    }

    private static void desconectarse_de_BBDD(Connection connection) {

        try {

            connection.close();

        } catch (SQLException ex) {

            mostrarMensajeError(ex.getMessage(), "Error SQL");

        }

    }

    public static boolean select_fila_BBDD(String tabla, String clave) throws Exception {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet;

        String senteciaSQL = "SELECT * FROM " + tabla + " WHERE clave = ?";
        preparedStatement = connection.prepareStatement(senteciaSQL);

        preparedStatement.setString(1, clave);

        resultSet = preparedStatement.executeQuery();

        //A partir de este ResultSet podriamos obtener Objectos del tipo de
        //Datos seleccionado
        if (resultSet.next()) { // Existe

            return true;

        } else { // No existe

            return false;

        }

    }

    public static void delete_fila_BBDD(String tabla, String clave) throws Exception {

        PreparedStatement preparedStatement = null;

        String senteciaSQL = "DELETE FROM " + tabla + " WHERE email = ?";

        preparedStatement = connection.prepareStatement(senteciaSQL);

        preparedStatement.setString(1, clave);

        preparedStatement.executeUpdate();
    }

    public static void insert_fila_BBDD(String tabla, Prueba prueba) throws Exception {
        //Comprueba si existe el email(clave primaria) para hacer UPDATE o INSERT
        boolean existe = select_fila_BBDD(tabla, prueba.getDato1());

        String sentenciaSQL;

        if (existe) {

            sentenciaSQL = "UPDATE " + tabla + " SET campo=? WHERE clave=?";

        } else {

            sentenciaSQL = "INSERT INTO " + tabla + " VALUES (?,?)";

        }

        PreparedStatement prepareStatement = connection.prepareStatement(sentenciaSQL);

        prepareStatement.setString(1, "valor_Campo");
        prepareStatement.setString(2, "valor_Clave");

        prepareStatement.executeUpdate();
    }

    public static ArrayList<Prueba> listar_tabla_toArrayList(String tabla) throws SQLException {

        ArrayList<Prueba> listado = new ArrayList<>();

        Statement statement = connection.createStatement();

        String sentenciaSQL = "SELECT * FROM " + tabla;

        ResultSet resultSet = statement.executeQuery(sentenciaSQL);

        while (resultSet.next()) {
            // Crea objeto Persona y lo añade a ArrayList
            listado.add(new Prueba(resultSet.getString("dato1"), resultSet.getString("dato2"), resultSet.getString("dato3")));
        }

        return listado;
    }

    //INTERFAZ GRAFICO    
    public static void mostrarMensajeError(String mensaje, String cabecera) {

        JOptionPane.showMessageDialog(null, mensaje, cabecera, JOptionPane.ERROR_MESSAGE);

    }

    public static boolean mostrarMensaje_Si_o_No(String mensaje, String cabecera) {

        if (JOptionPane.showConfirmDialog(null, mensaje, cabecera,
                JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
            return true;
        } else {
            return false;
        }
    }

    private static void leerContenidoFichero(String ruta) {

        try {

            File fichero = new File(ruta);

            FileReader fileReader = new FileReader(fichero);

            BufferedReader bufferDelLector = new BufferedReader(fileReader);

            String lineaFichero;
            String contenidoFichero = null;

            while ((lineaFichero = bufferDelLector.readLine()) != null) {

                contenidoFichero += lineaFichero + "\n";

            }

            bufferDelLector.close();

        } catch (FileNotFoundException ex) {

            mostrarMensajeError(ex.getMessage(), "Error File Not Found");

        } catch (IOException ex) {

            mostrarMensajeError(ex.getMessage(), "I/O Exception");

        }
    }

    private static void guardarContenidoFichero(String contenido) {
        
        try {
            
            FileWriter fileWriter  = null;
            PrintWriter printWriter = null;
            File fichero = new File("RUTA DEL FICHERO");
            
            fileWriter = new FileWriter(fichero.getAbsolutePath());//Anadir True al final para hacerlo en modo Append
            
            printWriter = new PrintWriter(fileWriter, false);
            
            //AQUI SE INTRODUCEN LOS DATOS EN UN BUCLE SI ES NECESARIO
            printWriter.println(contenido);
            
            printWriter.close();
            
        } catch (IOException ex) {
                        
            mostrarMensajeError(ex.getMessage(), "Error I/O");
            
        }
    }

}
